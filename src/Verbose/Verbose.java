package Verbose;

/**
 * Created by mads on 3/23/16.
 */
public class Verbose {
    public static boolean verbose;

    /**
     * Simple print if verbose is true
     * @param str
     */
    public static void print(String str) {
        if (verbose) {
            System.out.println(str);
        }
    }
}
