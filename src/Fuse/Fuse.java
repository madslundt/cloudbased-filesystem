package Fuse;

import Cryptography.Encoding;
import Key.Key;
import Key.KeyRing;
import net.fusejna.DirectoryFiller;
import net.fusejna.ErrorCodes;
import net.fusejna.FuseException;
import net.fusejna.StructFuseFileInfo.FileInfoWrapper;
import net.fusejna.StructStat.StatWrapper;
import net.fusejna.types.TypeMode.ModeWrapper;
import net.fusejna.types.TypeMode.NodeType;
import net.fusejna.util.FuseFilesystemAdapterAssumeImplemented;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class Fuse extends FuseFilesystemAdapterAssumeImplemented {
   private File _localRootDir;
    private final MemoryDirectory rootDirectory = new MemoryDirectory("");
    private KeyRing _keyRing;
    private Key _key;
    private static boolean _changedFile = false;
    private static boolean _nonCommitedFiles = false;

    //TODO evt. smide fejl hvis dirs ikke eksistere.
    public Fuse(String localRootDir, Key key) {
        this._localRootDir = new File(localRootDir);
        if (!_localRootDir.isDirectory()) {
            _localRootDir.mkdir();
        }
        this._key = key;
    }
    // Downloading filekeys and files using the list of localkeys.
    private void _loadFileKeys () {
        System.out.println("FileKeys and Files download & Decrypt begun.");

        File encryptedKeyRing = this._key.download(this._localRootDir.getPath(), true);
        File decryptedKeyRing = new File(this._localRootDir.getPath() + "/" + this._key.getReference() + ".xml");
        decryptedKeyRing = this._key.decrypt(encryptedKeyRing, decryptedKeyRing, true);
        this._keyRing = KeyRing.loadXML(decryptedKeyRing.getPath());

        this._keyRing.downloadAll(this._localRootDir.getPath(), true);

        System.out.println("FileKeys and Files download & decrypt finished.");
    }

    // Method moving a file from one point to another
    private boolean _moveFile(String from, String to) {
        Path movefrom = FileSystems.getDefault().getPath(from);
        Path target = FileSystems.getDefault().getPath(to);
        try {
            Files.move(movefrom, target, REPLACE_EXISTING);
        } catch (IOException e) {
            return false;
        }
        return true;
    }
    // Adding all the files downloaded earlier. The files are deleted as the are only temporarily
    private void _addFilesDir(String fileDir) {
        System.out.println("Adding files to mounted directory has begun.");
        File filesFolder = new File(fileDir);
        for (File file : filesFolder.listFiles()) {
            // Checks whether the file is a directory
            if (!file.isDirectory()) {
                try {
                    this.rootDirectory.add(new MemoryFile(file.getName(), file));
                    file.delete();
                } catch (IOException e) {
                    e.printStackTrace();
                    continue;
                }
            }
        }
        System.out.println("Adding files to the mounted directory finished.");
    }

    // Methods created for adding some test files
    private void addFiles() {
        this.rootDirectory.add(new MemoryFile("Sample file.txt", "Hello there, feel free to look around.\n"));
        // Outcommented some ways to create files and directories inside the mounted folder
        /*rootDirectory.add(new MemoryDirectory("Sample directory"));
        final MemoryDirectory dirWithFiles = new MemoryDirectory("Directory with files");
        rootDirectory.add(dirWithFiles);
        dirWithFiles.add(new MemoryFile("hello.txt", "This is some sample text.\n"));
        dirWithFiles.add(new MemoryFile("hello again.txt", "This another file with text in it! Oh my!\n"));
        final MemoryDirectory nestedDirectory = new MemoryDirectory("Sample nested directory");
        dirWithFiles.add(nestedDirectory);
        nestedDirectory.add(new MemoryFile("So deep.txt", "Man, I'm like, so deep in this here file structure.\n"));*/

    }

    /*private synchronized boolean _uploadFile(String file, String fileName) {
        System.out.println("Attempting to upload file: " + file);
        fileName = fileName.substring(1); // Removing the "/" from the file
        LocalKey lk = _keyRing.getLocalKey(fileName);
        if (lk == null) {
            System.out.println("Local key for file: " + fileName + " didn't exist.");
            return false;
        }
        System.out.println(lk.getMediaWiki().getUrl());
        lk.downloadFilekey();
        lk.decryptFileKey();
        FileKey fileKey = FileKey.loadXML(lk.getId() + ".xml");
        fileKey.setDecryptionFile(file);
        fileKey.encryptFile(".box");
        return true;
    }*/

    public void mount() throws FuseException {
        System.setProperty("jna.nosys", "true");
        String rootdirPath = _localRootDir.getPath();
        if (!_localRootDir.exists()) {
            _localRootDir.mkdir();
        }
        if (!_isDirEmpty(rootdirPath)) {
            System.out.println(rootdirPath + " is not empty. The root directory folder needs to be empty.\nConsider choosing a new rootdir by changing the Config.xml.\nMounting of " + rootdirPath + " Canceled.");
            return;
        }
        _loadFileKeys();
        this._addFilesDir(rootdirPath);
        this.mount(_localRootDir.getPath());
    }

    // Private method used for checking if a given directory is empty.
    private boolean _isDirEmpty(String dir) {
        Path directory = Paths.get(dir);
        try {
            DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory);
            return !dirStream.iterator().hasNext();
        } catch (IOException e) {
            System.out.println("Checking directory " + dir + " gives IOException.");
        }
        return false;
    }

    @Override
    public int access(final String path, final int access) {
        return 0;
    }

    @Override
    public int create(final String path, final ModeWrapper mode, final FileInfoWrapper info) {
        if (getPath(path) != null) {
            return -ErrorCodes.EEXIST();
        }
        final MemoryPath parent = getParentPath(path);
        if (parent instanceof MemoryDirectory) {
            ((MemoryDirectory) parent).mkfile(getLastComponent(path));
            return 0;
        }
        return -ErrorCodes.ENOENT();
    }

    @Override
    public int getattr(final String path, final StatWrapper stat) {
        final MemoryPath p = getPath(path);
        if (p != null) {
            p.getattr(stat);
            return 0;
        }
        return -ErrorCodes.ENOENT();
    }

    private String getLastComponent(String path) {
        while (path.substring(path.length() - 1).equals("/")) {
            path = path.substring(0, path.length() - 1);
        }
        if (path.isEmpty()) {
            return "";
        }
        return path.substring(path.lastIndexOf("/") + 1);
    }

    private MemoryPath getParentPath(final String path) {
        return rootDirectory.find(path.substring(0, path.lastIndexOf("/")));
    }

    private MemoryPath getPath(final String path) {
        return rootDirectory.find(path);
    }

    @Override
    public int mkdir(final String path, final ModeWrapper mode) {
        if (getPath(path) != null) {
            return -ErrorCodes.EEXIST();
        }
        final MemoryPath parent = getParentPath(path);
        if (parent instanceof MemoryDirectory) {
            ((MemoryDirectory) parent).mkdir(getLastComponent(path));
            return 0;
        }
        return -ErrorCodes.ENOENT();
    }

    @Override
    public int open(final String path, final FileInfoWrapper info) {
        return 0;
    }

    @Override
    public int read(final String path, final ByteBuffer buffer, final long size, final long offset, final FileInfoWrapper info) {
        final MemoryPath p = getPath(path);
        if (p == null) {
            return -ErrorCodes.ENOENT();
        }
        if (!(p instanceof MemoryFile)) {
            return -ErrorCodes.EISDIR();
        }
        return ((MemoryFile) p).read(buffer, size, offset);
    }

    @Override
    public int readdir(final String path, final DirectoryFiller filler) {
        final MemoryPath p = getPath(path);
        if (p == null) {
            return -ErrorCodes.ENOENT();
        }
        if (!(p instanceof MemoryDirectory)) {
            return -ErrorCodes.ENOTDIR();
        }
        ((MemoryDirectory) p).read(filler);
        return 0;
    }

    @Override
    public int rename(final String path, final String newName) {
        final MemoryPath p = getPath(path);
        if (p == null) {
            return -ErrorCodes.ENOENT();
        }
        final MemoryPath newParent = getParentPath(newName);
        if (newParent == null) {
            return -ErrorCodes.ENOENT();
        }
        if (!(newParent instanceof MemoryDirectory)) {
            return -ErrorCodes.ENOTDIR();
        }
        p.delete();
        p.rename(newName.substring(newName.lastIndexOf("/")));
        ((MemoryDirectory) newParent).add(p);
        // Looking if the file is changed
        System.out.println("Rename " + newName);
        System.out.println(_changedFile);
        if (_changedFile) {
            _nonCommitedFiles = false;
            _changedFile = false;
            System.out.println("File change detected: Committing file " + _localRootDir + newName);
            //TODO Actually commit the file. This only works when changing Files in gedit
            //this._uploadFile(_localRootDir + newName, newName);
        } else if (!(_nonCommitedFiles && _changedFile)) { // Looking for filename changes that isn't a part of a program save file rename
            System.out.println("Rename detected: Updating LocalKey FileName from " + path + " to " + newName + ".");
            String filePath = path;
            String newFilePath = newName;
            if (path.startsWith("/")) {
                filePath = path.substring(1);
            }
            if (newName.startsWith("/")) {
                newFilePath = newName.substring(1);
            }

            // k1 --> o1 --> k2 --> o2 --> k3 --> file
            // kX: Key
            // oX: Key ring
            Key key = this._keyRing.getKeyByFilename(filePath); // This finds key k3 which is pointing to a file
            System.out.println(key.getId() + " --> " + key.getReference());
            key.setName(newFilePath); // Change name in key

            KeyRing keyRing = this._keyRing.getKeyRingContainingKey(key.getId()); // Find key ring o2 containing key k3
            Key refKey;
            if (this._key.getReference().equals(keyRing.getId())) {
                refKey = this._key;
            } else {
                refKey = this._keyRing.getKeyReferingToKeyRing(keyRing.getId()); // Find key k2 pointing to key ring o2
                System.out.println(refKey.getId());
            }
            System.out.println(keyRing.getId() + " --> " + refKey.getId());

            keyRing.addKey(key, true);
            refKey.encryptAndUpload(keyRing);
            //TODO Actually change the LocalKey
        }
        return 0;
    }

    @Override
    public int rmdir(final String path) {
        final MemoryPath p = getPath(path);
        if (p == null) {
            return -ErrorCodes.ENOENT();
        }
        if (!(p instanceof MemoryDirectory)) {
            return -ErrorCodes.ENOTDIR();
        }
        p.delete();
        return 0;
    }

    @Override
    public int truncate(final String path, final long offset) {
        final MemoryPath p = getPath(path);
        if (p == null) {
            return -ErrorCodes.ENOENT();
        }
        if (!(p instanceof MemoryFile)) {
            return -ErrorCodes.EISDIR();
        }
        ((MemoryFile) p).truncate(offset);
        return 0;
    }

    @Override
    public int unlink(final String path) {
        final MemoryPath p = getPath(path);
        if (p == null) {
            return -ErrorCodes.ENOENT();
        }
        p.delete();
        return 0;
    }

    @Override
    public int write(final String path, final ByteBuffer buf, final long bufSize, final long writeOffset,
                     final FileInfoWrapper wrapper) {
        final MemoryPath p = getPath(path);
        if (p == null) {
            return -ErrorCodes.ENOENT();
        }
        if (!(p instanceof MemoryFile)) {
            return -ErrorCodes.EISDIR();
        }
        return ((MemoryFile) p).write(buf, bufSize, writeOffset);
    }

    @Override
    public int fsync(String path, int datasync, FileInfoWrapper info) {
        System.out.println("fsync");
        System.out.println("fsync");
        _changedFile = true;
        return 0;
    }

    private final class MemoryDirectory extends MemoryPath {
        private final List<MemoryPath> contents = new ArrayList<MemoryPath>();

        private MemoryDirectory(final String name) {
            super(name);
        }

        private MemoryDirectory(final String name, final MemoryDirectory parent) {
            super(name, parent);
        }

        public synchronized void add(final MemoryPath p) {
            contents.add(p);
            p.parent = this;
        }

        private synchronized void deleteChild(final MemoryPath child) {
            contents.remove(child);
        }

        @Override
        protected MemoryPath find(String path) {
            if (super.find(path) != null) {
                return super.find(path);
            }
            while (path.startsWith("/")) {
                path = path.substring(1);
            }
            synchronized (this) {
                if (!path.contains("/")) {
                    for (final MemoryPath p : contents) {
                        if (p.name.equals(path)) {
                            return p;
                        }
                    }
                    return null;
                }
                final String nextName = path.substring(0, path.indexOf("/"));
                final String rest = path.substring(path.indexOf("/"));
                for (final MemoryPath p : contents) {
                    if (p.name.equals(nextName)) {
                        return p.find(rest);
                    }
                }
            }
            return null;
        }

        @Override
        protected void getattr(final StatWrapper stat) {
            stat.setMode(NodeType.DIRECTORY);
        }

        private synchronized void mkdir(final String lastComponent) {
            contents.add(new MemoryDirectory(lastComponent, this));
        }

        public synchronized void mkfile(final String lastComponent) {
            contents.add(new MemoryFile(lastComponent, this));
        }

        public synchronized void read(final DirectoryFiller filler) {
            for (final MemoryPath p : contents) {
                filler.add(p.name);
            }
        }
    }

    private final class MemoryFile extends MemoryPath {
        private ByteBuffer contents = ByteBuffer.allocate(0);

        private MemoryFile(final String name) {
            super(name);
        }

        private MemoryFile(final String name, final MemoryDirectory parent) {
            super(name, parent);
        }

        public MemoryFile(final String name, final String text) {
            super(name);
            try {
                final byte[] contentBytes = text.getBytes("UTF-8");
                contents = ByteBuffer.wrap(contentBytes);
            } catch (final UnsupportedEncodingException e) {
                // Not going to happen
            }
        }

        public MemoryFile(final String name, final File file) throws IOException {
            super(name);
            try {
                byte[] contentBytes = Files.readAllBytes(Paths.get(file.getPath()));
                contentBytes = Encoding.decode(Encoding.encode(contentBytes));
                contents = ByteBuffer.wrap(contentBytes);
            } catch (final UnsupportedEncodingException e) {
                // Not going to happen
            }
            //System.out.println("New file " + name);
        }

        @Override
        protected void getattr(final StatWrapper stat) {
            stat.setMode(NodeType.FILE).size(contents.capacity());
        }

        private int read(final ByteBuffer buffer, final long size, final long offset) {
            final int bytesToRead = (int) Math.min(contents.capacity() - offset, size);
            final byte[] bytesRead = new byte[bytesToRead];
            synchronized (this) {
                contents.position((int) offset);
                contents.get(bytesRead, 0, bytesToRead);
                buffer.put(bytesRead);
                contents.position(0); // Rewind
            }
            return bytesToRead;
        }

        private synchronized void truncate(final long size) {
            if (size < contents.capacity()) {
                // Need to create a new, smaller buffer
                final ByteBuffer newContents = ByteBuffer.allocate((int) size);
                final byte[] bytesRead = new byte[(int) size];
                contents.get(bytesRead);
                newContents.put(bytesRead);
                contents = newContents;
            }
        }

        private int write(final ByteBuffer buffer, final long bufSize, final long writeOffset) {
            final int maxWriteIndex = (int) (writeOffset + bufSize);
            final byte[] bytesToWrite = new byte[(int) bufSize];
            synchronized (this) {
                if (maxWriteIndex > contents.capacity()) {
                    // Need to create a new, larger buffer
                    final ByteBuffer newContents = ByteBuffer.allocate(maxWriteIndex);
                    newContents.put(contents);
                    contents = newContents;
                    //System.out.println("New contents?");
                }
                buffer.get(bytesToWrite, 0, (int) bufSize);
                contents.position((int) writeOffset);
                contents.put(bytesToWrite);
                contents.position(0); // Rewind
            }
            return (int) bufSize;
        }
    }

    private abstract class MemoryPath {
        private String name;
        private MemoryDirectory parent;

        private MemoryPath(final String name) {
            this(name, null);
        }

        private MemoryPath(final String name, final MemoryDirectory parent) {
            this.name = name;
            this.parent = parent;
        }

        private synchronized void delete() {
            if (parent != null) {
                parent.deleteChild(this);
                parent = null;
            }
        }

        protected MemoryPath find(String path) {
            while (path.startsWith("/")) {
                path = path.substring(1);
            }
            if (path.equals(name) || path.isEmpty()) {
                return this;
            }
            return null;
        }

        protected abstract void getattr(StatWrapper stat);

        private void rename(String newName) {
            while (newName.startsWith("/")) {
                newName = newName.substring(1);
            }
            name = newName;
        }
    }


}