/**
 * Copyright (C) 2015 BITPlan GmbH
 *
 * Pater-Delp-Str. 1
 * D-47877 Willich-Schiefbahn
 *
 * http://www.bitplan.com
 * 
 * This source is part of
 * https://github.com/WolfgangFahl/Mediawiki-Japi
 * and the license for Mediawiki-Japi applies
 * 
 */
package Mediawiki.japi;

import Mediawiki.japi.api.*;
import Mediawiki.japi.api.Error;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;

/**
 * common implementation parts
 * @author wf
 *
 */
public abstract class MediaWikiApiImpl implements MediawikiApi {
  
  /**
   * Logging may be enabled by setting debug to true
   */
  protected static java.util.logging.Logger LOGGER = java.util.logging.Logger
      .getLogger("com.bitplan.mediawiki.japi");

  /**
   * set to true if exceptions should be thrown on Error
   */
  protected boolean throwExceptionOnError = true;
  
  /**
   * protection Marker - if this shows in  page edits are suppressed and logged with a warning
   */
  protected String protectionMarker="<!-- This page is protected against edits by Mediawiki-Japi -->";
  
  /**
   * @return the throwExceptionOnError
   */
  public boolean isThrowExceptionOnError() {
    return throwExceptionOnError;
  }

  /**
   * @param throwExceptionOnError
   *          the throwExceptionOnError to set
   */
  public void setThrowExceptionOnError(boolean throwExceptionOnError) {
    this.throwExceptionOnError = throwExceptionOnError;
  }
  
  /**
   * @return the protectionMarker
   */
  public String getProtectionMarker() {
    return protectionMarker;
  }

  /**
   * @param protectionMarker the protectionMarker to set
   */
  public void setProtectionMarker(String protectionMarker) {
    this.protectionMarker = protectionMarker;
  }
  
  /**
   * handle the given error Message according to the exception setting
   * 
   * @param errMsg
   * @throws Exception
   */
  protected void handleError(String errMsg) throws Exception {
    // log it
    LOGGER.log(Level.SEVERE, errMsg);
    // and throw an error if this is configured
    if (this.isThrowExceptionOnError()) {
      throw new Exception(errMsg);
    }
  }
  
  /**
   * handle the given api error
   * @param error
   * @throws Exception
   */
  protected void handleError(Error error) throws Exception {
    String errMsg="error: "+error.getCode()+" info: "+error.getInfo();
    handleError(errMsg);
  }
  
  /**
   * 
   * @param api
   * @throws Exception
   */
  protected void handleError(Api api) throws Exception {
    if (api==null) {
      String errMsg="api result is null";
      handleError(errMsg);
    }
    if (api.getError() != null) {
      this.handleError(api.getError());
    }
    if (api.getWarnings() != null) {
      Warnings warnings = api.getWarnings();
      if (warnings.getTokens() != null) {
        Tokens warningTokens = warnings.getTokens();
        String errMsg = warningTokens.getValue();
        handleError(errMsg);
      }
    }
  }
  
  /**
   * return Api from the given xml
   * @param xml - the xml go unmarshal
   * @return
   * @throws Exception
   */
  public Api fromXML(String xml) throws Exception {
    Api api;
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Api.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      api = (Api) jaxbUnmarshaller.unmarshal(new StringReader(xml));
    } catch (Exception ex) {
      api = new Api();
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      DocumentBuilder db = dbf.newDocumentBuilder();
      InputSource e = new InputSource(new StringReader(xml));
      Document doc = db.parse(e);
      XPath xPath = XPathFactory.newInstance().newXPath();
      XPathExpression expr = xPath.compile("api/*");
      NodeList nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
      for (int i = 0; i < nl.getLength(); i++) {
        final Node node = nl.item(i);
        if (node.getNodeName().equals("login")) {
          api = this._getLoginApi(node);
        } else if (node.getNodeName().equals("query")) {
          System.out.println(xml);
          api = this._getQueryApi(node);
        } else if (node.getNodeName().equals("upload")) {
          System.out.println(xml);
          //api = this._getUploadApi(node);
        } else if (node.getNodeName().equals("edit")) {
          System.out.println(xml);
          //api = this._getEditApi(node);
        }
      }
    }

    // retrieve the JAXB wrapper representation from the xml received
    // check whether an error code was sent
    Error error = api.getError();
    // if there is an error - handle it
    if (error != null) {
      // prepare the error message
      String errMsg = "error code=" + error.getCode() + " info:'"
          + error.getInfo() + "'";
      this.handleError(errMsg);
    }
    return api;
  }

  private Api _getLoginApi(Node node) {
    NamedNodeMap attr = node.getAttributes();
    Api api = new Api();
    Login login = new Login();
    login.setResult(_tryGetContent(attr.getNamedItem("result")));
    login.setCookieprefix(_tryGetContent(attr.getNamedItem("cookieprefix")));
    login.setSessionid(_tryGetContent(attr.getNamedItem("sessionid")));

    if (_tryGetContent(attr.getNamedItem("result")).equalsIgnoreCase("needtoken")) {
      login.setToken(_tryGetContent(attr.getNamedItem("token")));
    } else if (_tryGetContent(attr.getNamedItem("result")).equalsIgnoreCase("success")) {
      login.setLgusername(_tryGetContent(attr.getNamedItem("lgusername")));
      login.setLgtoken(_tryGetContent(attr.getNamedItem("lgtoken")));
      login.setLguserid(_tryGetContent(attr.getNamedItem("lguserid")));
    }

    api.setLogin(login);
    return api;
  }

  private Api _getQueryApi(Node queryNode) throws Exception {
    System.out.println(queryNode.getNodeName());
    Api api = new Api();
    Query query = new Query();
    NodeList nodes = queryNode.getChildNodes();
    for (int i = 0; i < nodes.getLength(); i++) {
      queryNode = nodes.item(i);
      if (queryNode.getNodeName().equalsIgnoreCase("general")) {
        NamedNodeMap attr = queryNode.getAttributes();
        General general = new General();
        general.setMainpage(_tryGetContent(attr.getNamedItem("mainpage")));
        general.setBase(_tryGetContent(attr.getNamedItem("base")));
        general.setSitename(_tryGetContent(attr.getNamedItem("sitename")));
        general.setLogo(_tryGetContent(attr.getNamedItem("logo")));
        general.setGenerator(_tryGetContent(attr.getNamedItem("generator")));
        general.setPhpversion(_tryGetContent(attr.getNamedItem("phpversion")));
        general.setPhpsapi(_tryGetContent(attr.getNamedItem("phpsapi")));
        general.setDbtype(_tryGetContent(attr.getNamedItem("dbtype")));
        general.setDbversion(_tryGetContent(attr.getNamedItem("dbversion")));
        general.setImagewhitelistenabled(_tryGetContent(attr.getNamedItem("imagewhitelistenabled")));
        general.setLangconversion(_tryGetContent(attr.getNamedItem("langconversion")));
        general.setTitleconversion(_tryGetContent(attr.getNamedItem("titleconversion")));
        general.setLinkprefixcharset(_tryGetContent(attr.getNamedItem("linkprefixcharset")));
        general.setLinkprefix(_tryGetContent(attr.getNamedItem("linkprefix")));
        general.setLinktrail(_tryGetContent(attr.getNamedItem("linktrail")));
        general.setLegaltitlechars(_tryGetContent(attr.getNamedItem("legaltitlechars")));
        general.setCase(_tryGetContent(attr.getNamedItem("case")));
        general.setLang(_tryGetContent(attr.getNamedItem("lang")));
        general.setFallback8BitEncoding(_tryGetContent(attr.getNamedItem("fallback8bitEncoding")));
        general.setWriteapi(_tryGetContent(attr.getNamedItem("writeapi")));
        general.setTimezone(_tryGetContent(attr.getNamedItem("timezone")));
        general.setTimeoffset(Byte.parseByte(_tryGetContent(attr.getNamedItem("timeoffset"))));
        general.setArticlepath(_tryGetContent(attr.getNamedItem("articlepath")));
        general.setScriptpath(_tryGetContent(attr.getNamedItem("scriptpath")));
        general.setScript(_tryGetContent(attr.getNamedItem("script")));
        general.setServer(_tryGetContent(attr.getNamedItem("server")));
        general.setServername(_tryGetContent(attr.getNamedItem("servername")));
        general.setWikiid(_tryGetContent(attr.getNamedItem("wikiid")));
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss");
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(format.parse(_tryGetContent(attr.getNamedItem("time"))));
        general.setTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
        general.setMaxuploadsize(Integer.parseInt(_tryGetContent(attr.getNamedItem("maxuploadsize"))));
        general.setFavicon(_tryGetContent(attr.getNamedItem("favicon")));

        query.setGeneral(general);
      } else if (queryNode.getNodeName().equalsIgnoreCase("tokens")) {
        NamedNodeMap attr = queryNode.getAttributes();
        Tokens token = new Tokens();
        token.setCsrftoken(_tryGetContent(attr.getNamedItem("csrftoken")));
        query.setTokens(token);
      } else if (queryNode.getNodeName().equalsIgnoreCase("pages")) {
        NodeList pageNodes = queryNode.getChildNodes();
        List<Page> pages = new ArrayList<Page>();
        for (int j = 0; j < pageNodes.getLength(); j++) {
          Node child = pageNodes.item(j);
          NamedNodeMap pageAttr = child.getAttributes();
          Page page = new Page();
          String pageid = _tryGetContent(pageAttr.getNamedItem("pageid"));
          if (pageid != null) {
            page.setPageid(Short.parseShort(pageid));
          }
          page.setNs(Integer.parseInt(_tryGetContent(pageAttr.getNamedItem("ns"))));
          page.setTitle(_tryGetContent(pageAttr.getNamedItem("title")));
          Node c = child.getFirstChild();
          List<Rev> revisions = new ArrayList<Rev>();
          Imageinfo image = new Imageinfo();
          if (c != null && c.getNodeName().equalsIgnoreCase("imageinfo")) {
            Ii ii = new Ii();
            Node n = c.getFirstChild();
            NamedNodeMap iiAttr = n.getAttributes();
            DateFormat format = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss");
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(format.parse(_tryGetContent(iiAttr.getNamedItem("timestamp"))));
            ii.setTimestamp(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
            ii.setUser(_tryGetContent(iiAttr.getNamedItem("user")));
            ii.setUserid(Integer.parseInt(_tryGetContent(iiAttr.getNamedItem("userid"))));
            ii.setSize(Integer.parseInt(_tryGetContent(iiAttr.getNamedItem("size"))));
            ii.setWidth(Short.parseShort(_tryGetContent(iiAttr.getNamedItem("width"))));
            ii.setHeight(Short.parseShort(_tryGetContent(iiAttr.getNamedItem("height"))));
            ii.setParsedcomment(_tryGetContent(iiAttr.getNamedItem("parsedocument")));
            ii.setComment(_tryGetContent(iiAttr.getNamedItem("commment")));
            ii.setUrl(_tryGetContent(iiAttr.getNamedItem("url")));
            ii.setDescriptionurl(_tryGetContent(iiAttr.getNamedItem("descriptionurl")));
            ii.setSha1(_tryGetContent(iiAttr.getNamedItem("sha1")));
            ii.setMime(_tryGetContent(iiAttr.getNamedItem("mime")));
            ii.setMediatype(_tryGetContent(iiAttr.getNamedItem("mediatype")));
            ii.setBitdepth(new BigInteger(_tryGetContent(iiAttr.getNamedItem("bitdepth"))));

            image.setIi(ii);
          } else if (c != null && c.getNodeName().equalsIgnoreCase("revisions")) {
            NodeList children = c.getChildNodes();
            for (int k = 0; k < children.getLength(); k++) {
              Node n = children.item(k);
              NamedNodeMap revAttr = n.getAttributes();
              Rev revision = new Rev();
              revision.setContentformat(_tryGetContent(revAttr.getNamedItem("contentformat")));
              revision.setContentmodel(_tryGetContent(revAttr.getNamedItem("contentmodel")));
              revision.setValue(_tryGetContent(n));
              revisions.add(revision);
            }
          } else if (c != null) {
            System.out.println("Wrong: " + c.getNodeName());
          }

          page.setRevisions(revisions);
          page.setImageinfo(image);
          pages.add(page);
        }
        query.setPages(pages);
      } else if (queryNode.getNodeName().equalsIgnoreCase("namespaces")) {
        NodeList children = queryNode.getChildNodes();
        List<Ns> namespaces = new ArrayList<Ns>();
        for (int j = 0; j < children.getLength(); j++) {
          Node n = children.item(j);
          NamedNodeMap nsAttr = n.getAttributes();
          Ns ns = new Ns();
          ns.setId(Integer.parseInt(_tryGetContent(nsAttr.getNamedItem("id"))));
          ns.setCase(_tryGetContent(nsAttr.getNamedItem("case")));
          ns.setCanonical(_tryGetContent(nsAttr.getNamedItem("canonical")));
          ns.setSubpages(_tryGetContent(nsAttr.getNamedItem("subpages")));
          ns.setContent(_tryGetContent(n));
          namespaces.add(ns);
        }
        query.setNamespaces(namespaces);
      } else {
        System.out.println("WRONG: " + queryNode.getNodeName());
      }
    }
    api.setQuery(query);
    return api;
  }
  private Api _getUploadApi(Node node) {
    return null;
  }
  private Api _getEditApi(Node node) {
    return null;
  }

  private String _tryGetContent(Node n) {
    if (n != null && n.getTextContent() != null) {
      return n.getTextContent();
    } else {
      return null;
    }
  }
  
  // we do not implement this ... 
  public abstract SiteInfo getSiteInfo() throws Exception;
  
  /**
   * copy the page for a given title from this wiki to the given target Wiki
   * uses https://www.mediawiki.org/wiki/API:Edit FIXME - make this an API
   * interface function FIXME - create a multi title version
   * 
   * @param targetWiki
   *          - the other wiki (could use a different API implementation ...)
   * @param pageTitle
   *          - the title of the page to copy
   * @param summary
   *          - the summary to use
   * @return - the Edit result
   * @throws Exception
   */
  public Edit copyToWiki(MediawikiApi targetWiki, String pageTitle,
      String summary) throws Exception {
    SiteInfo siteinfo = this.getSiteInfo();
    SiteInfo targetSiteInfo = targetWiki.getSiteInfo();
    PageInfo sourcePageInfo=new PageInfo(pageTitle,siteinfo);
    String targetPageTitle=pageTitle;
    Edit result=null;
    String nameSpaceName="";
    if (sourcePageInfo.namespace!=null) {
      nameSpaceName=sourcePageInfo.nameSpaceName;
      String targetNameSpace=siteinfo.mapNamespace(nameSpaceName, targetSiteInfo);
      targetPageTitle=pageTitle.replaceFirst(nameSpaceName+":",targetNameSpace+":");
    }
    String content = getPageContent(pageTitle);
    // "File:" namespace (ID: 6) used see http://www.mediawiki.org/wiki/Manual:Namespace#Built-in_namespaces
    if (sourcePageInfo.namespaceId==6) {
      // get the image information 
      Ii ii = this.getImageInfo(pageTitle);
      String filename=pageTitle.replaceFirst("File:","");
      if (nameSpaceName!=null) {
        filename=filename.replaceFirst(nameSpaceName+":", "");
      }
      targetWiki.upload(ii,filename,content);
      result=new Edit();
      result.setTitle(filename);
    } else {
      result = targetWiki.edit(targetPageTitle, content, summary);
    }
    return result;
  }
  
  /**
   * get the Version of this wiki
   * 
   * @throws Exception
   */
  public String getVersion() throws Exception {
    String mediawikiVersion=getSiteInfo().getVersion();
    return mediawikiVersion;
  }
  /**
   * request parameter encoding
   * 
   * @param param
   * @return an encoded url parameter
   * @throws Exception
   */
  protected String encode(String param) throws Exception {
    String result = URLEncoder.encode(param, "UTF-8");
    return result;
  }

  /**
   * normalize the given page title
   * 
   * @param title
   * @return the normalized title e.g. replacing blanks FIXME encode is not good
   *         enough
   * @throws Exception
   */
  public String normalizeTitle(String title) throws Exception {
    String result = encode(title);
    result=result.replace("+","_");
    return result;
  }
}
