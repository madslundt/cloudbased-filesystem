package Mediawiki.japi.api;

public class Revision {
    protected String url;
    protected String content;
    protected String timestamp;

    public Revision(String url, String content) {
        this.url = url;
        this.content = content;
    }

    public String getUrl() {
        return this.url;
    }

    public String getContent() {
        return this.content;
    }

    public String getTimestamp() {
        return this.timestamp;
    }
}
