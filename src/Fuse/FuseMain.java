package Fuse;

import Config.Config;
import Key.Key;
import Key.KeyRing;
import net.fusejna.FuseException;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by novaz on 4/28/16.
 */
public class FuseMain {

    /****
     * This class and method is only for testing purposes, before a real test class.
     * @param args
     */
   public static void main(String[] args) {
        Config config = Config.loadConfig();
        File keyFile = new File(config.getLocalKeyPath());
        Key key = Key.loadKeyFromXML(keyFile);
        if (key == null) {
            File file = new File(System.getProperty("user.dir") + "/src/TestFiles/test.txt");
            Key tempKey = new Key(file);
            tempKey.encryptAndUpload(file);
            KeyRing keyRing = new KeyRing(new ArrayList<Key>() {{
                add(tempKey);
            }});
            key = new Key(keyRing);

            key.saveXML(keyFile);

            key.encryptAndUpload(keyRing);
        }
        Fuse mountFolder = new Fuse(config.getRootDirectory(), key);
        try {
            mountFolder.mount();
        } catch (FuseException e) {
            e.printStackTrace();
        }
    }
}