package Cryptography;

import org.apache.commons.io.IOUtils;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.util.Arrays;
/**
 * Created by Mads Lundt on 3/26/16.
 */
public class Symmetric {
    private static final String _PADDING_SCHEME = "AES/CBC/PKCS5Padding";
    private final String _MESSAGE_DIGEST_CRYTOGRAPHY_ALGORITHM = "SHA-256";
    private static final String _KEY_SPEC_ALGORITHM = "AES";
    private static final int _BYTES = 32; // 256 bit

    private SecretKeySpec _secretKey;
    private IvParameterSpec _iv;

    /**
     * Creates a symmetric key from a password and iv.
     * @param password
     * @param iv
     */
    public Symmetric(String password, String iv) {
        byte[] key = new byte[0];
        try {
            key = password.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        MessageDigest sha = null;
        try {
            sha = MessageDigest.getInstance(this._MESSAGE_DIGEST_CRYTOGRAPHY_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        key = sha.digest(key);
        key = Arrays.copyOf(key, this._BYTES); // use only first 256 bit
        this._secretKey = new SecretKeySpec(key, this._KEY_SPEC_ALGORITHM);
        this._iv = new IvParameterSpec(Encoding.decode(iv));
    }

    /**
     * Encrypts plainText with the symmetric key.
     * @param plainText
     * @return
     */
    public String encrypt(String plainText) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance(this._PADDING_SCHEME);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        try {
            cipher.init(Cipher.ENCRYPT_MODE, this._secretKey, this._iv);
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }

        byte[] cipherData;
        if (Encoding.isEncoded(plainText)) {
            cipherData = cipher.doFinal(Encoding.decode(plainText));
        } else {
            try {
                cipherData = cipher.doFinal(plainText.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null;
            }
        }
        return Encoding.encode(cipherData);
    }
    public void encrypt(InputStream in, OutputStream out) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        try {
            Cipher cipher = Cipher.getInstance(this._PADDING_SCHEME);
            cipher.init(Cipher.ENCRYPT_MODE, this._secretKey, this._iv);

            out = new CipherOutputStream(out, cipher);
            IOUtils.copy(in, out);

            in.close();
            out.close();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Decrypts cipherText with the symmetric key.
     * @param cipherText
     * @return
     */
    public String decrypt(String cipherText) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance(this._PADDING_SCHEME);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        try {
            cipher.init(Cipher.DECRYPT_MODE, this._secretKey, this._iv);
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return Encoding.encode(cipher.doFinal(Encoding.decode(cipherText)));
    }

    public void decrypt(InputStream in, OutputStream out) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        try {
            Cipher cipher = Cipher.getInstance(this._PADDING_SCHEME);
            cipher.init(Cipher.DECRYPT_MODE, this._secretKey, this._iv);

            in = new CipherInputStream(in, cipher);
            IOUtils.copy(in, out);

            in.close();
            out.close();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public SealedObject encrypt(Serializable object) {
        try {
            Cipher cipher = Cipher.getInstance(_PADDING_SCHEME);
            cipher.init(Cipher.ENCRYPT_MODE, this._secretKey, this._iv);
            SealedObject sealedObject = new SealedObject(object, cipher);

            return sealedObject;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
    public Object decrypt(SealedObject encryptedObject) {
        try {
            Cipher cipher = Cipher.getInstance(_PADDING_SCHEME);
            cipher.init(Cipher.DECRYPT_MODE, this._secretKey, this._iv);
            Object decryptedObject = encryptedObject.getObject(cipher);

            return decryptedObject;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Generates a secure random key.
     * @return
     */
    public static String generateKey() {
        final SecureRandom r = new SecureRandom();
        byte[] key = new byte[_BYTES];
        r.nextBytes(key);

        return Encoding.encode(key);
    }

    /**
     * Generates a secure random iv.
     * @return
     */
    public static String generateIv() {
        final SecureRandom r = new SecureRandom();
        byte[] key = new byte[_BYTES / 2];
        r.nextBytes(key);

        return Encoding.encode(key);
    }
}
