package Cryptography;

import org.junit.Assert;
import org.junit.Test;

import java.nio.charset.Charset;

/**
 * Created by mads on 4/19/16.
 */
public class AsymmetricTest {

    @Test
    public void getPublicKey() throws Exception {
        Asymmetric asym = new Asymmetric();
        Assert.assertNotNull(asym.getPublicKey());
        Assert.assertTrue(asym.getPublicKey().length() > 0);

        Asymmetric asym2 = new Asymmetric(asym.getPublicKey(), asym.getPrivateKey());
        Assert.assertEquals(asym2.getPublicKey(), asym.getPublicKey());
    }

    @Test
    public void getPrivateKey() throws Exception {
        Asymmetric asym = new Asymmetric();
        Assert.assertNotNull(asym.getPrivateKey());
        Assert.assertTrue(asym.getPrivateKey().length() > 0);

        Asymmetric asym2 = new Asymmetric(asym.getPublicKey(), asym.getPrivateKey());
        Assert.assertEquals(asym2.getPrivateKey(), asym.getPrivateKey());
    }

    @Test
    public void generateUniqueKeys() throws Exception {
        Asymmetric asym1 = new Asymmetric();
        Asymmetric asym2 = new Asymmetric();

        Assert.assertNotEquals(asym1.getPublicKey(), asym2.getPublicKey());
        Assert.assertNotEquals(asym1.getPrivateKey(), asym2.getPrivateKey());
    }

    @Test
    public void encryptUsingPublicKey() throws Exception {
        String plainText = "Test123";

        Asymmetric asym = new Asymmetric();
        String encryptedText = asym.encryptByUsingPublicKey(plainText);

        Assert.assertNotNull(encryptedText);
        Assert.assertTrue(encryptedText.length() > 0);
        Assert.assertNotEquals(plainText, encryptedText);
    }

    @Test
    public void decryptUsingPrivateKey() throws Exception {
        String plainText = "Test123";

        Asymmetric asym = new Asymmetric();
        String encryptedText = asym.encryptByUsingPublicKey(plainText);

        String decryptedText = new String(Encoding.decode(asym.decryptByUsingPrivateKey(encryptedText)), Charset.forName("UTF-8"));

        Assert.assertNotNull(decryptedText);
        Assert.assertTrue(decryptedText.length() > 0);
        Assert.assertNotEquals(encryptedText, decryptedText);
        Assert.assertEquals(plainText, decryptedText);
    }

    @Test
    public void encryptUsingPrivateKey() throws Exception {
        String plainText = "Test123";

        Asymmetric asym = new Asymmetric();
        String encryptedText = asym.encryptByUsingPrivateKey(plainText);

        Assert.assertNotNull(encryptedText);
        Assert.assertTrue(encryptedText.length() > 0);
        Assert.assertNotEquals(plainText, encryptedText);
    }

    @Test
    public void decryptUsingPublicKey() throws Exception {
        String plainText = "Test123";

        Asymmetric asym = new Asymmetric();
        String encryptedText = asym.encryptByUsingPrivateKey(plainText);

        String decryptedText = new String(Encoding.decode(asym.decryptByUsingPublicKey(encryptedText)), Charset.forName("UTF-8"));

        Assert.assertNotNull(decryptedText);
        Assert.assertTrue(decryptedText.length() > 0);
        Assert.assertNotEquals(encryptedText, decryptedText);
        Assert.assertEquals(plainText, decryptedText);
    }
}