package Cryptography;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
/**
 * Created by Mads Lundt on 3/26/16.
 */
public class Asymmetric {
    private final String _CRYPTOGRAPHY_ALGORITHM = "RSA";
    private final int _KEY_LENGTH = 2048;

    private final String _CIPHER_ENCRYPTING = "RSA/ECB/PKCS1Padding";

    private String _publicKey;
    private String _privateKey;

    /**
     * Creates an asymmetric key pair with private- and public key.
     */
    public Asymmetric() {
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance(this._CRYPTOGRAPHY_ALGORITHM);
            kpg.initialize(this._KEY_LENGTH);
            KeyPair kp = kpg.generateKeyPair();
            this._publicKey = Encoding.encode(kp.getPublic().getEncoded());
            this._privateKey = Encoding.encode(kp.getPrivate().getEncoded());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initialize an asymmetric key pair with only the public key.
     * @param publicKey
     */
    public Asymmetric(String publicKey) {
        this._publicKey = publicKey;
    }

    /**
     * Initialize an asymmetric key pair with private- and public key.
     * @param publicKey
     * @param privateKey
     */
    public Asymmetric(String publicKey, String privateKey) {
        this._publicKey = publicKey;
        this._privateKey = privateKey;
    }

    /**
     * Returns the public key (Base64 encoded)
     * @return
     */
    public String getPublicKey() {
        return this._publicKey;
    }

    /**
     * Returns the private key (Base64 encoded)
     * @return
     */
    public String getPrivateKey() {
        return this._privateKey;
    }

    /**
     * Encrypts plainText with its public key
     * @param plainText
     * @return
     */
    public String encryptByUsingPublicKey(String plainText) throws InvalidKeySpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance(this._CIPHER_ENCRYPTING);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        cipher.init(Cipher.ENCRYPT_MODE, this._getPublicKey());

        int maxLength = this._KEY_LENGTH / 8;
        if (plainText.length() > maxLength) {
            int chunks = (int) Math.ceil((double) plainText.length() / maxLength);
            String cipherData = "";
            for (int i = 0; i < chunks; i++) {
                String text = plainText.substring(i * maxLength, Math.min((i+1) * maxLength, plainText.length()));

                byte[] cipherChunk = cipher.doFinal(Encoding.decode(text));
                cipherData += Encoding.encode(cipherChunk) + " ";
            }
            return cipherData.trim();
        } else {
            byte[] cipherData;
            if (Encoding.isEncoded(plainText)) {
                cipherData = cipher.doFinal(Encoding.decode(plainText));
            } else {
                try {
                    cipherData = cipher.doFinal(plainText.getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            return Encoding.encode(cipherData);
        }
    }

    /**
     * Decrypts cipherText with its private key
     * @param cipherText
     * @return
     */
    public String decryptByUsingPrivateKey(String cipherText) throws InvalidKeySpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance(this._CIPHER_ENCRYPTING);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        cipher.init(Cipher.DECRYPT_MODE, this._getPrivateKey());
        int maxLength = this._KEY_LENGTH / 8;
        if (cipherText.length() > maxLength && cipherText.contains(" ")) {
            String[] chunks = cipherText.split(" ");
            String cipherData = "";
            for (String chunk : chunks) {
                cipherData += Encoding.encode(cipher.doFinal(Encoding.decode(chunk)));
            }
            return cipherData;
        } else {
            return Encoding.encode(cipher.doFinal(Encoding.decode(cipherText)));
        }
    }

    /**
     * Encrypts plainText with its private key
     * @param plainText
     * @return
     */
    public String encryptByUsingPrivateKey(String plainText) throws InvalidKeySpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance(this._CIPHER_ENCRYPTING);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        cipher.init(Cipher.ENCRYPT_MODE, this._getPrivateKey());

        int maxLength = this._KEY_LENGTH / 8;
        if (plainText.length() > maxLength) {
            int chunks = (int) Math.ceil((double) plainText.length() / maxLength);
            String cipherData = "";
            for (int i = 0; i < chunks; i++) {
                String text = plainText.substring(i * maxLength, Math.min((i+1) * maxLength, plainText.length()));
                byte[] cipherChunk = cipher.doFinal(Encoding.decode(text));
                cipherData += Encoding.encode(cipherChunk) + " ";
            }
            return cipherData.trim();
        } else {
            byte[] cipherData;
            if (Encoding.isEncoded(plainText)) {
                cipherData = cipher.doFinal(Encoding.decode(plainText));
            } else {
                try {
                    cipherData = cipher.doFinal(plainText.getBytes("UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            return Encoding.encode(cipherData);
        }
    }

    /**
     * Decrypts cipherText with its public key
     * @param cipherText
     * @return
     */
    public String decryptByUsingPublicKey(String cipherText) throws InvalidKeySpecException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance(this._CIPHER_ENCRYPTING);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
        cipher.init(Cipher.DECRYPT_MODE, this._getPublicKey());

        int maxLength = this._KEY_LENGTH / 8;
        if (cipherText.length() > maxLength && cipherText.contains(" ")) {
            String[] chunks = cipherText.split(" ");
            String cipherData = "";
            for (String chunk : chunks) {
                cipherData += Encoding.encode(cipher.doFinal(Encoding.decode(chunk)));
            }
            return cipherData;
        } else {
            return Encoding.encode(cipher.doFinal(Encoding.decode(cipherText)));
        }
    }

    /**
     * Re-creates the public key from a string
     * @return
     */
    private PublicKey _getPublicKey() throws InvalidKeySpecException {
        byte[] bytes = Encoding.decode(this._publicKey);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
        KeyFactory kf = null;
        try {
            kf = KeyFactory.getInstance(this._CRYPTOGRAPHY_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return kf.generatePublic(keySpec);
    }

    /**
     * Re-creates the private key from a string
     * @return
     */
    private PrivateKey _getPrivateKey() throws InvalidKeySpecException {
        byte[] bytes = Encoding.decode(this._privateKey);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
        KeyFactory kf = null;
        try {
            kf = KeyFactory.getInstance(this._CRYPTOGRAPHY_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return kf.generatePrivate(keySpec);
    }
}
