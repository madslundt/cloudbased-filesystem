package Cryptography;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.io.File;

/**
 * Created by mads on 4/19/16.
 */
public class SymmetricTest {

    @Test
    public void encrypt() throws Exception {
        String plainText = "Test123";
        Symmetric sym = new Symmetric(Symmetric.generateKey(), Symmetric.generateIv());
        String encryptedText = sym.encrypt(plainText);

        Assert.assertNotNull(encryptedText);
        Assert.assertNotEquals(plainText, encryptedText);
    }
    @Test
    public void decrypt() throws Exception {
        String plainText = "Test123";
        Symmetric sym = new Symmetric(Symmetric.generateKey(), Symmetric.generateIv());
        String encryptedText = sym.encrypt(plainText);

        String decryptedText = sym.decrypt(encryptedText);
        decryptedText = new String(Encoding.decode(decryptedText), Charset.forName("UTF-8"));
        Assert.assertNotNull(decryptedText);
        Assert.assertEquals(plainText, decryptedText);
    }
    @Test
    public void encryptFile() throws Exception {
        Symmetric sym = new Symmetric(Symmetric.generateKey(), Symmetric.generateIv());
        File outputEncryptedFile = new File("src/TestFiles/test2.txt");
        File outputDecryptedFile = new File("src/TestFiles/test.txt");
        InputStream in = new FileInputStream(outputDecryptedFile);
        OutputStream out = new FileOutputStream(outputEncryptedFile);
        sym.encrypt(in, out);

        Assert.assertNotEquals(Hashing.generateFileHash(outputEncryptedFile), Hashing.generateFileHash(outputDecryptedFile));

        outputEncryptedFile.delete();
    }
    @Test
    public void decryptFile() throws Exception {
        Symmetric sym = new Symmetric(Symmetric.generateKey(), Symmetric.generateIv());
        File outputEncryptedFile = new File("src/TestFiles/test2.txt");
        File originalFile = new File("src/TestFiles/test.txt");
        InputStream in = new FileInputStream(originalFile);
        OutputStream out = new FileOutputStream(outputEncryptedFile);
        sym.encrypt(in, out);

        File outputDecryptedFile = new File("src/TestFiles/testDecrypted.txt");
        in = new FileInputStream(outputEncryptedFile);
        out = new FileOutputStream(outputDecryptedFile);
        sym.decrypt(in, out);
        in.close();
        out.close();

        Assert.assertEquals(Hashing.generateFileHash(originalFile), Hashing.generateFileHash(outputDecryptedFile));

        outputDecryptedFile.delete();
        outputEncryptedFile.delete();

    }

    @Test
    public void generateKey() throws Exception {
        String key = Symmetric.generateKey();
        Assert.assertNotNull(key);
        Assert.assertTrue(key.length() > 0);
    }

    @Test
    public void generateIv() throws Exception {
        String iv = Symmetric.generateIv();
        Assert.assertNotNull(iv);
        Assert.assertTrue(iv.length() > 0);
    }

    @Test
    public void generateUniqueKeys() throws Exception {
        Assert.assertNotEquals(Symmetric.generateKey(), Symmetric.generateKey());
    }
    @Test
    public void generateUniqueIvs() throws Exception {
        Assert.assertNotEquals(Symmetric.generateIv(), Symmetric.generateIv());
    }
}