package Cryptography;

import org.junit.Assert;
import org.junit.Test;

import java.nio.charset.Charset;

/**
 * Created by mads on 4/19/16.
 */
public class EncodingTest {

    @Test
    public void encode() throws Exception {
        String text = "Test123";
        String encodedText = Encoding.encode(text.getBytes("UTF-8"));

        Assert.assertFalse(Encoding.isEncoded(text));
        Assert.assertNotNull(encodedText);
        Assert.assertTrue(encodedText.length() > 0);
        Assert.assertNotEquals(text, encodedText);
        Assert.assertTrue(Encoding.isEncoded(encodedText));
    }

    @Test
    public void decode() throws Exception {
        String text = "Test123";
        String encodedText = Encoding.encode(text.getBytes("UTF-8"));

        String decodedText = new String(Encoding.decode(encodedText), Charset.forName("UTF-8"));

        Assert.assertNotNull(decodedText);
        Assert.assertTrue(decodedText.length() > 0);
        Assert.assertNotEquals(decodedText, encodedText);
        Assert.assertEquals(text, decodedText);
        Assert.assertFalse(Encoding.isEncoded(decodedText));
    }
}