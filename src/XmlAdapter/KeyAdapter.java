package XmlAdapter;

import Key.Key;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by madslundt on 29/03/16.
 */
public class KeyAdapter extends XmlAdapter<KeyList, Map<String, Key>> {
    @Override
    public Map<String, Key> unmarshal(KeyList value ) {
        Map<String, Key> map = new HashMap<>();
        for (Key key : value.keys) {
            map.put(key.getId(), key);
        }
        return map;
    }

    @Override
    public KeyList marshal(Map<String, Key> map ) {
        KeyList keyList = new KeyList();
        Collection<Key> keys = map.values();
        keyList.keys = keys.toArray(new Key[keys.size()]);
        return keyList;
    }
}

class KeyList {
    @XmlElement(name="key")
    public Key[] keys;
}