package Key;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mads on 5/14/16.
 */
public class KeyRingTest {
    private File _file;

    @Before
    public void setUp() throws Exception {
        this._file = new File(System.getProperty("user.dir") + "/src/TestFiles/test.txt");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void initKeyRingList() {
        KeyRing keyRing = new KeyRing(new ArrayList<Key>());
        Assert.assertNotNull(keyRing.getId());
        Assert.assertFalse(keyRing.getId().isEmpty());
    }
    @Test
    public void initKeyRingHashmap() {
        KeyRing keyRing = new KeyRing(new HashMap<String, Key>());
        Assert.assertNotNull(keyRing.getId());
        Assert.assertFalse(keyRing.getId().isEmpty());
    }
    @Test
    public void addKey() {
        KeyRing keyRing = new KeyRing(new ArrayList<Key>());
        Key key = new Key(_file);
        Assert.assertEquals(keyRing.getKeys().size(), 0);
        Assert.assertNull(keyRing.getKey(key.getId()));

        keyRing.addKey(key);

        Assert.assertEquals(keyRing.getKeys().size(), 1);
        Assert.assertNotNull(keyRing.getKey(key.getId()));
    }
    @Test
    public void getKey() {
        Key key = new Key(_file);
        KeyRing keyRing = new KeyRing(new ArrayList<Key>() {{
            add(key);
        }});Assert.assertEquals(keyRing.getKeys().size(), 1);
        Assert.assertNotNull(keyRing.getKey(key.getId()));

        Assert.assertEquals(keyRing.getKeys().size(), 1);
        Assert.assertNotNull(keyRing.getKey(key.getId()));
    }

}